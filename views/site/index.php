<?php

/* @var $this yii\web\View */

use yii\helpers\Url;
$this->title = 'Просмотр курса валют';
?>
<div class="site-index">
<?php if(\Yii::$app->user->can('viewCurrency')){ ?>
    <div class="jumbotron">
        <h1>Здравствуйте, <?= Yii::$app->user->identity->username ?>!</h1>
        <p class="lead">Для просмотра графика динамики курса валют перейдите по <a href="<?= Url::to(['site/currency'])?>">ссылке</a>.</p>
    </div>
<?php } else { ?>
    <div class="jumbotron">
        <h1>Здравствуйте!</h1>
        <p class="lead">Для просмотра графика динамики курса валют Вам необходимо <a href="<?= Url::to(['site/login'])?>">авторизоваться</a> с правами администратора.</p>
    </div>
<?php } ?>
</div>
<?php

/* @var $this yii\web\View */

use app\assets\ChartsAsset;
ChartsAsset::register($this);

$this->title = 'My Yii Application';
?>
<div class="site-index">
    <div class="jumbotron">
        <p class="lead">График динамики курса валют:</p>

        <div id="container" style="min-width: 400px; height: 400px; margin: 0 auto"></div>

        <script>
            Highcharts.chart('container', {
                chart: {
                    type: 'line'
                },
                title: {
                    text: 'Weekly currency'
                },
                subtitle: {
                    text: 'Source: api.privatbank.ua'
                },
                xAxis: {
                    categories: <?= $categories ?>
                },
                yAxis: {
                    title: {
                        text: 'Value (UAH)'
                    }
                },
                plotOptions: {
                    line: {
                        dataLabels: {
                            enabled: true
                        },
                        enableMouseTracking: false
                    }
                },
                series: <?= $series ?>
            });
        </script>
    </div>
</div>
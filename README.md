О проекте
---------
Особенности:

Источником курсов валют был выбран https://api.privatbank.ua/p24api/exchange_rates?json&date=DATE, где DATE - это дата в формате дд.мм.гггг

API сайта minfin.com.ua требует ждать подтверждения заявки и имеет временные ограничения, поэтому как пример использовать не совсем удобно.

Для реализации авторизации на сайте был использован стандартный функционал фреймворка Yii2 - Контроль доступа на основе ролей (RBAC).

Была создана консольная команда, которая инициализирует данные через APIs, предоставляемые authManager'ом (commands/RbacController).
В ней была создана роль 'admin', которой была назначена под роль - 'viewCurrency'. Для роли 'admin' назначен пользователь admin. Созданы 4 таблицы, которые использует DbManager.

Уровень прав viewCurrency предпологает доступ к разделу курсов валют на сайте (разрешение настроено в методе behaviors контроллера SiteController).

Была создана консольная команда GeneratePasswordHashController для быстрой генерации хэша пароля с помощью Yii::$app->getSecurity()->generatePasswordHash().

Так как график курса валют имеет js скрипты, которые должны быть загружены строго в порядке и после jquery, был создан отдельный файл ChartsAsset, в котором указаны эти два скрипта и место их вывода ('position' => \yii\web\View::POS_BEGIN).

На будущее этот график можно сделать отдельным виджетом.

Используя конфигурацию DB по умолчанию, я добавил файл db.php в .gitignore с целью защиты данных подключения. Создал файл db_example.php и добавил его в репозиторий как пример для копирования.

Настройки адреса для парсинга курса валют и количество дней я вынес в config/params. 

Были созданы 3 миграции: создание таблицы user, добавления в нее админа, создание таблицы курсов валют (сохраняются для примера три валюты). 

После первой загрузки данных, они сохраняются в таблицу currency. По умолчанию парсинг данных начинается с даты: текущая дата - 3 недели, так как свежие данные могут отсутствовать на сайте. Если вдруг скрипт не получает данные нужных валют, выпадает Exception('Wrong count of currency data.'); 

В будущем эту ситуацию нужно обработать корректно, сделано исключительно для тестового отображения.

Для оптимизации я бы добавил кэширование запросов к БД, схем БД, HTTP кэширование. Добавил бы возможность выбора валют, даты и упростил код.

Также есть идея вынести запрос к API в отдельную команду (как GeneratePasswordHashController), который будет принимать в параметрах - дату и сохранять (если нужно) данные в БД.

Хорошего дня :) !

DIRECTORY STRUCTURE
-------------------

      assets/             contains assets definition
      commands/           contains console commands (controllers)
      config/             contains application configurations
      controllers/        contains Web controller classes
      mail/               contains view files for e-mails
      models/             contains model classes
      runtime/            contains files generated during runtime
      tests/              contains various tests for the basic application
      vendor/             contains dependent 3rd-party packages
      views/              contains view files for the Web application
      web/                contains the entry script and Web resources



REQUIREMENTS
------------

The minimum requirement by this project template that your Web server supports PHP 5.4.0.


INSTALLATION
------------

### Install via Composer

If you do not have [Composer](http://getcomposer.org/), you may install it by following the instructions
at [getcomposer.org](http://getcomposer.org/doc/00-intro.md#installation-nix).

You can then install this project template using the following command:

~~~
php composer.phar create-project --prefer-dist --stability=dev yiisoft/yii2-app-basic basic
~~~

Now you should be able to access the application through the following URL, assuming `basic` is the directory
directly under the Web root.

~~~
http://localhost/basic/web/
~~~

### Install from an Archive File

Extract the archive file downloaded from [yiiframework.com](http://www.yiiframework.com/download/) to
a directory named `basic` that is directly under the Web root.

Set cookie validation key in `config/web.php` file to some random secret string:

```php
'request' => [
    // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
    'cookieValidationKey' => '<secret random string goes here>',
],
```

You can then access the application through the following URL:

~~~
http://localhost/basic/web/
~~~


CONFIGURATION
-------------

### Database

Edit the file `config/db.php` with real data, for example:

```php
return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=localhost;dbname=yii2basic',
    'username' => 'root',
    'password' => '1234',
    'charset' => 'utf8',
];
```

**NOTES:**
- Yii won't create the database for you, this has to be done manually before you can access it.
- Check and edit the other files in the `config/` directory to customize your application as required.
- Refer to the README in the `tests` directory for information specific to basic application tests.


TESTING
-------

Tests are located in `tests` directory. They are developed with [Codeception PHP Testing Framework](http://codeception.com/).
By default there are 3 test suites:

- `unit`
- `functional`
- `acceptance`

Tests can be executed by running

```
vendor/bin/codecept run
```

The command above will execute unit and functional tests. Unit tests are testing the system components, while functional
tests are for testing user interaction. Acceptance tests are disabled by default as they require additional setup since
they perform testing in real browser. 


### Running  acceptance tests

To execute acceptance tests do the following:  

1. Rename `tests/acceptance.suite.yml.example` to `tests/acceptance.suite.yml` to enable suite configuration

2. Replace `codeception/base` package in `composer.json` with `codeception/codeception` to install full featured
   version of Codeception

3. Update dependencies with Composer 

    ```
    composer update  
    ```

4. Download [Selenium Server](http://www.seleniumhq.org/download/) and launch it:

    ```
    java -jar ~/selenium-server-standalone-x.xx.x.jar
    ```

    In case of using Selenium Server 3.0 with Firefox browser since v48 or Google Chrome since v53 you must download [GeckoDriver](https://github.com/mozilla/geckodriver/releases) or [ChromeDriver](https://sites.google.com/a/chromium.org/chromedriver/downloads) and launch Selenium with it:

    ```
    # for Firefox
    java -jar -Dwebdriver.gecko.driver=~/geckodriver ~/selenium-server-standalone-3.xx.x.jar
    
    # for Google Chrome
    java -jar -Dwebdriver.chrome.driver=~/chromedriver ~/selenium-server-standalone-3.xx.x.jar
    ``` 
    
    As an alternative way you can use already configured Docker container with older versions of Selenium and Firefox:
    
    ```
    docker run --net=host selenium/standalone-firefox:2.53.0
    ```

5. (Optional) Create `yii2_basic_tests` database and update it by applying migrations if you have them.

   ```
   tests/bin/yii migrate
   ```

   The database configuration can be found at `config/test_db.php`.


6. Start web server:

    ```
    tests/bin/yii serve
    ```

7. Now you can run all available tests

   ```
   # run all available tests
   vendor/bin/codecept run

   # run acceptance tests
   vendor/bin/codecept run acceptance

   # run only unit and functional tests
   vendor/bin/codecept run unit,functional
   ```

### Code coverage support

By default, code coverage is disabled in `codeception.yml` configuration file, you should uncomment needed rows to be able
to collect code coverage. You can run your tests and collect coverage with the following command:

```
#collect coverage for all tests
vendor/bin/codecept run -- --coverage-html --coverage-xml

#collect coverage only for unit tests
vendor/bin/codecept run unit -- --coverage-html --coverage-xml

#collect coverage for unit and functional tests
vendor/bin/codecept run functional,unit -- --coverage-html --coverage-xml
```

You can see code coverage output under the `tests/_output` directory.

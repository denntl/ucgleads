<?php

use yii\db\Migration;

class m171213_160346_insert_user_data extends Migration
{
    public function up()
    {
        $this->insert('user', [
            'username' => 'admin',
            'password' => Yii::$app->getSecurity()->generatePasswordHash('admin'),
            'authKey' => Yii::$app->security->generateRandomString(),
        ]);
    }

    public function down()
    {
        $this->delete('user', ['username' => 'admin']);
    }
}

<?php

use yii\db\Migration;

/**
 * Handles the creation of table `currency`.
 */
class m171213_170340_create_currency_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('currency', [
            'id' => $this->primaryKey(),
            'date' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP'),
            'rub' => $this->float(64)->notNull(),
            'usd' => $this->float(64)->notNull(),
            'eur' => $this->float(64)->notNull(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('currency');
    }
}

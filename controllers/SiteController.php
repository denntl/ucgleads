<?php

namespace app\controllers;

use app\models\Currency;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\base\Exception;

class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'currency'],
                'rules' => [
                    [
                        'actions' => ['currency'],
                        'allow' => true,
                        'roles' => ['viewCurrency'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays currency page.
     *
     * @return Response|string
     */
    public function actionCurrency()
    {
        $chartCategories = [];
        $chartData = [
            'USD' => [],
            'EUR' => [],
            'RUB' => [],
        ];

        $threeWeeksAgoTime = strtotime( date("Y-m-d") . " -3 weeks");

        for($day = 0; $day < Yii::$app->params['period']; $day++){

            $time = $threeWeeksAgoTime + 24 * 3600 * $day;
            $dateSiteFormat = date("d.m.Y", $time);
            $dateDbFormat = date("Y.m.d", $time);

            $currencyDb = Currency::findOne(['date' => $dateDbFormat]);

            if(! $currencyDb) {

                $currencyData = $this->getCurrencyData($dateSiteFormat, $dateDbFormat, $chartData);
                if($currencyData){

                    $chartCategories[] = $dateSiteFormat;
                    $chartData['USD'][] = $currencyData['USD'];
                    $chartData['EUR'][] = $currencyData['EUR'];
                    $chartData['RUB'][] = $currencyData['RUB'];
                }

            } else {

                $chartCategories[] = $dateSiteFormat;
                $chartData['USD'][] = $currencyDb->usd;
                $chartData['EUR'][] = $currencyDb->eur;
                $chartData['RUB'][] = $currencyDb->rub;
            }
        }

        $series = [
            (object) [
                "name" => "USD",
                "data" => $chartData['USD']
            ],
            (object) [
                "name" => "EUR",
                "data" => $chartData['EUR']
            ],
            (object) [
                "name" => "RUB",
                "data" => $chartData['RUB']
            ]
        ];

        return $this->render('currency', [
            'categories' => json_encode($chartCategories),
            'series' => json_encode($series)
        ]);
    }

    public function getCurrencyData($dateSiteFormat, $dateDbFormat, $chartData){

        $currencyData = [];

        $responseStr = file_get_contents(Yii::$app->params['apiUrl'] . $dateSiteFormat);
        $responseArr = json_decode($responseStr, true);

        if (isset($responseArr['exchangeRate'])) {
            foreach ($responseArr['exchangeRate'] as $currency) {
                if (isset( $chartData[$currency['currency']])) {
                    $currencyData[$currency['currency']] = $currency['saleRateNB'];
                }
            }
            if (count($currencyData) === count($chartData)) {
                $currency = new Currency();
                $currency->date = $dateDbFormat;
                $currency->usd = $currencyData['USD'];
                $currency->eur = $currencyData['EUR'];
                $currency->rub = $currencyData['RUB'];
                $currency->save();
            } else {
                throw new Exception('Wrong count of currency data.');
            }
        }

        return $currencyData;
    }
}

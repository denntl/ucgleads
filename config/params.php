<?php

return [
    'adminEmail' => 'admin@example.com',
    'apiUrl' => 'https://api.privatbank.ua/p24api/exchange_rates?json&date=',
    'period' => 7
];

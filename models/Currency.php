<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "currency".
 *
 * @property integer $id
 * @property string $date
 * @property double $rub
 * @property double $usd
 * @property double $eur
 */
class Currency extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'currency';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date'], 'safe'],
            [['rub', 'usd', 'eur'], 'required'],
            [['rub', 'usd', 'eur'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'date' => 'Date',
            'rub' => 'Rub',
            'usd' => 'Usd',
            'eur' => 'Eur',
        ];
    }
}

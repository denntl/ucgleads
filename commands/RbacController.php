<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use Yii;
use yii\console\Controller;

class RbacController extends Controller
{
    public function actionInit()
    {
        $auth = Yii::$app->authManager;

        $viewCurrency = $auth->createPermission('viewCurrency');
        $viewCurrency->description = 'Can view currency';
        $auth->add($viewCurrency);

        $admin = $auth->createRole('admin');
        $auth->add($admin);
        $auth->addChild($admin, $viewCurrency);

        $auth->assign($admin, 1);
    }
}

<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use Yii;
use yii\console\Controller;

class GeneratePasswordHashController extends Controller
{
    public $password;

    public function options($actionID)
    {
        return ['password'];
    }

    public function optionAliases()
    {
        return ['p' => 'password'];
    }

    public function actionIndex()
    {
        echo Yii::$app->getSecurity()->generatePasswordHash($this->password) . "\n";
    }
}

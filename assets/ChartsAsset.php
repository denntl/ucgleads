<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Charts asset bundle.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class ChartsAsset extends AssetBundle
{
    public $jsOptions = ['position' => \yii\web\View::POS_BEGIN];
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $js = [
        'js/highcharts.js',
        'js/exporting.js'
    ];
}
